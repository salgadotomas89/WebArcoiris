package com.example.demo2.controller;

import com.example.demo2.entity.Archivo;
import com.example.demo2.entity.Comentario;
import com.example.demo2.service.ArchivoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
public class ArchivoController {
    @Autowired
    ArchivoService archivoService;
    private final String UPLOAD_DIR = "home/escuela3/public_html/archivos/";
    private Long idArchivoModificado;
    private Long idUsuario;
    private int nivelCurso;
    private String tipoArchivo = "todo";

    @GetMapping("/comentario/{id}")
    public String updateComentario (@PathVariable Long id, Model model) {
        Comentario comentario = new Comentario();
        idArchivoModificado = id;
        comentario.setTexto(archivoService.get(id).getComentario());
        model.addAttribute("comen", comentario);
        return "add-comentario";
    }

    @GetMapping("/delete/comentario/{id}")
    public String deleteUser (@PathVariable Long id, Model model) {
        archivoService.delete(id);
        return "redirect:/board";
    }

    @GetMapping("/downloadfile/{id}")
    public ResponseEntity<Resource> downloadFile(@PathVariable Long id) throws IOException{
        Archivo dbFile = archivoService.get(id);
        InputStream targetStream = new FileInputStream(dbFile.getArchivo());
        InputStreamResource file = new InputStreamResource(targetStream);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + dbFile.getNombreArchivo())
                .contentType(MediaType.parseMediaType(dbFile.getContentType()))
                .body(file);
    }

    @GetMapping("/setnivel/{nivel}")
    public String cargarNivel(@PathVariable int nivel){
        nivelCurso = nivel;
        return "redirect:/board";
    }

    @GetMapping("/setuser/{iduser}")
    public String cargarNivel(@PathVariable Long iduser){
        idUsuario = iduser;
        return "redirect:/cursos";
    }

    @GetMapping("/settipo/{tipo}")
    public String cargarTipo(@PathVariable String tipo){
        tipoArchivo = tipo;
        return "redirect:/board";
    }


    @GetMapping("/board")
    public String tabla(Model model) {
        if (tipoArchivo.equals("todo")) {
            model.addAttribute("listaArchivos", archivoService.getAllByNivelId(nivelCurso, idUsuario));
        } else{
            model.addAttribute("listaArchivos", archivoService.getAllByNivelTipoId(nivelCurso, tipoArchivo,idUsuario));
        }
        model.addAttribute("lista", archivoService.getAllByNivelId(nivelCurso, idUsuario));
        return "board";
    }

    @PostMapping("/upload/comentario")
    public String setComentario(Comentario comentario, Model model){
        archivoService.updateComentario(comentario, idArchivoModificado);
        return "redirect:/board";
    }


    @RequestMapping("/archivos")
    public String mostrarArchivos(Model model) {
        model.addAttribute("listaArchivos", archivoService.getAll());
        return "archivos";
    }

    @GetMapping("/addfile")
    public String guardarArchivo(Model model) {
        model.addAttribute("archivo", new Archivo());
        return "form-archivo";
    }

    @PostMapping("/upload")
    public String uploadFile(@RequestParam("file") MultipartFile file, RedirectAttributes attributes, Archivo archivo, Model model) {

        // verificar si esta vacio
        if (file.isEmpty()) {
            attributes.addFlashAttribute("message", "Elija un archivo.");
            return "redirect:/addfile";
        }

        // normalize the file path
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        archivo.setContentType(file.getContentType());
        archivo.setFecha(dameFecha());
        archivo.setNivel(nivelCurso);
        archivo.setIdUsuario(idUsuario);
        archivo.setNombreArchivo(fileName);
        try {
            archivo.setArchivo(convert(file));
            /*Path path = Paths.get(UPLOAD_DIR + fileName);
            Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);*/
        } catch (IOException e) {
            e.printStackTrace();
        }


        this.archivoService.save(archivo);

        attributes.addFlashAttribute("message", "You successfully uploaded " + fileName + '!');

        return "redirect:/board";
    }

    private Date dameFecha() {
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        dateFormat.format(date);
        return date;
    }

    private File convert(MultipartFile file) throws IOException {
        File convFile = new File(file.getOriginalFilename());
        convFile.createNewFile();
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
    }

    private byte[] readFileToByteArray(File file){
        FileInputStream fis = null;
        // Creating a byte array using the length of the file
        // file.length returns long which is cast to int
        byte[] bArray = new byte[(int) file.length()];
        try{
            fis = new FileInputStream(file);
            fis.read(bArray);
            fis.close();

        }catch(IOException ioExp){
            ioExp.printStackTrace();
        }
        return bArray;
    }


}
