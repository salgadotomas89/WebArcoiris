package com.example.demo2.controller;

import com.example.demo2.entity.Curso;
import com.example.demo2.service.CursoService;
import com.example.demo2.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class CursoController {
    @Autowired
    UserService userService;
    @Autowired
    CursoService cursoService;


    @GetMapping("/cursos")
    public String cursos(Model model) {
        model.addAttribute("lista",cursoService.getAll());
        model.addAttribute("listaProfesores", userService.getAll());
        return "cursos";
    }

    @GetMapping("/savecurso/{id}")
    public String guardarCurso(@PathVariable("id") Long id, Model model) {
        if(id != null && id != 0){
            model.addAttribute("curso", cursoService.get(id));
        }else{
            model.addAttribute("curso", new Curso());
            model.addAttribute("listProfesores", userService.getAll());
        }
        return "add-curso";
    }

    @PostMapping("/savecurso")
    public String saveCurso(Curso curso, Model model) {
        this.cursoService.save(curso);
        return "redirect:/cursos";
    }


}
