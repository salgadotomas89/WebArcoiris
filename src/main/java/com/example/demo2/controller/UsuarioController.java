package com.example.demo2.controller;

import com.example.demo2.entity.Curso;
import com.example.demo2.entity.User;
import com.example.demo2.service.ArchivoService;
import com.example.demo2.service.CursoService;
import com.example.demo2.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.validation.Valid;

@Controller
public class UsuarioController implements WebMvcConfigurer {
    @Autowired
    UserService userService;
    @Autowired
    CursoService cursoService;

    @Autowired
    ArchivoService archivoService;


    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/usuarios").setViewName("usuarios");
    }
/*
    @GetMapping("/")
    public String showForm(User user) {
        return "add-user";
    }*/

    @PostMapping("/save")
    public String checkPersonInfo(@Valid User user, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "add-user";
        }else{
            this.userService.save(user);

        }
        return "redirect:/usuarios";
    }
/*
    @PostMapping("/save")
    public String save( User user, Model model) {
        this.userService.save(user);
        return "redirect:/profesores";
    }*/



    @GetMapping("/usuarios")
    public String profesores(Model model){
        model.addAttribute("list", userService.getAll());
        return "usuarios";
    }


    @GetMapping("/login")
    public String login(){
        return "login";
    }

    @RequestMapping("/")
    public String index(Model model) {
        model.addAttribute("list", userService.getAll());
        return "index";
    }


    @GetMapping("/save/{id}")
    public String showSave(@PathVariable("id") Long id, Model model) {
        if (id != null && id != 0) {
            model.addAttribute("user", userService.get(id));
        } else {
            model.addAttribute("user", new User());
        }
        return "add-user";
    }





    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id, Model model) {
        userService.delete(id);

        return "index";
    }




}
