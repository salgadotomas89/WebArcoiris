package com.example.demo2.entity;

import javax.persistence.Entity;

public class Comentario {

    public Long id;

    public String texto;

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
