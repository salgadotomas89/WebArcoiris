package com.example.demo2.entity;

import com.sun.istack.NotNull;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.persistence.*;

@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)//para el id automatico en la BD
    public Long id;

    @NotBlank(message = "Nombre es obligatorio")
    @Pattern(regexp = "[A-Za-z]+")
    public String nombre;

    @NotBlank(message = "Apellido es obligatorio")
    @Pattern(regexp = "[A-Za-z]+")
    public String apellido;

    @NotNull
    public String genero;

    @NotBlank(message = "Nombre de usuario es obligatorio")
    public String userName;

    @Email
    public String email;

    @NotBlank(message = "Contraseña es obligatoria")
    @Size(min=6, max=10)
    public String password;

    @NotBlank(message = "Profesion es obligatoria")
    @Pattern(regexp = "[A-Za-z]+")
    public String profesion;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String nombreusuario) {
        this.userName = nombreusuario;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String contraseña) {
        this.password = contraseña;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getProfesion() {
        return profesion;
    }

    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
