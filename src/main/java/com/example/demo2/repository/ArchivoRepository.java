
package com.example.demo2.repository;

import com.example.demo2.entity.Archivo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ArchivoRepository extends JpaRepository<Archivo, Long> {

    @Query("select c from Archivo c where c.nivel = :nivel")
    List<Archivo> findAllByNivel(@Param("nivel") int nivel);

    @Query("select c from Archivo c where c.nivel = :nivel and c.tipo = :tipo")
    List<Archivo> findAllByNivelAndTipo(@Param("nivel") int nivel, String tipo);

    @Query("select c from Archivo c where c.nivel = :nivel and c.tipo = :tipo and c.idUsuario = :id")
    Object findAllByNivelTipoId(@Param("nivel") int nivel, String tipo,@Param("id") Long id);

    @Query("select c from Archivo c where c.nivel = :nivel and c.idUsuario = :id")
    Object findAllByNivelId(@Param("nivel") int nivelCurso,@Param("id") Long id);
}
