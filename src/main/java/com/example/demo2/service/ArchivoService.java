package com.example.demo2.service;

import com.example.demo2.entity.Archivo;
import com.example.demo2.entity.Comentario;
import com.example.demo2.repository.ArchivoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class ArchivoService {
    @Autowired
    public ArchivoRepository archivoRepository;

    public Object getAll() {
        return archivoRepository.findAll();
    }

    public Object getAllByNivel(int nivel){
        return  archivoRepository.findAllByNivel(nivel);
    }

    public Object getAllByNivelAndTipo(int nivel, String tipo){
        return archivoRepository.findAllByNivelAndTipo(nivel, tipo);
    }

    public Object getAllByNivelTipoId(int nivel, String tipo, Long id){
        return archivoRepository.findAllByNivelTipoId(nivel, tipo, id);
    }

    public Archivo get(Long id) {
        return archivoRepository.findById(id).get();
    }

    public void save(Archivo archivo) {
        archivoRepository.save(archivo);
    }

    public void delete(Long id) {
        archivoRepository.deleteById(id);
    }

    public void updateComentario(Comentario comentario, Long id){
        Archivo archivo = archivoRepository.findById(id).get();
        archivo.setComentario(comentario.getTexto());
        archivoRepository.save(archivo);
    }


    public Object getAllByNivelId(int nivelCurso, Long idUsuario) {
        return archivoRepository.findAllByNivelId(nivelCurso, idUsuario);
    }

}
