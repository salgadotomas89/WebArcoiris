package com.example.demo2.service;

import com.example.demo2.entity.User;
import com.example.demo2.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    public UserRepository userRepository;


    public Object getAll() {
        return userRepository.findAll();
    }

    public Object get(Long id) {
        return userRepository.findById(id);
    }

    public void save(User user) {
        userRepository.save(user );
    }

    public void delete(Long id) {
        userRepository.deleteById(id);
    }
}
